import Vue from 'vue';
declare module 'vue/types/vue' {
    interface Vue {
        $hostname: string;
        $websocket: string;
        $images: string;
        $auth: useAuth;
        $lexique: ElementLexique[];
    }

    interface ElementLexique {
        original: string;
        hanyu: string;
        pinyin: string;
        kanji: string;
        romanji: string;
        hangeul: string;
        romanisationCoreen: string;
        description: string;
    }
}
