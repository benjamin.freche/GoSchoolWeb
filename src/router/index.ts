import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import {
    authGuard,
    authGuardAdmin,
    authGuardProfesseur
} from '../auth/authGuard';
// import Home from "../views/Home.vue";
import Accueil from '@/views/Accueil.vue';
import CommentCaMarche from '@/views/commun/CommentCaMarche.vue';
import Profil from '@/views/Compte/Profil.vue';
import ListeLibertes from '@/views/ModeJeu/libertes/ListeLibertes.vue';
import LireProblemeLibertes from '@/views/ModeJeu/libertes/LireProblemeLibertes.vue';
import ListeTesuji from '@/views/ModeJeu/tesuji/ListeTesuji.vue';
import LireTesuji from '@/views/ModeJeu/tesuji/LireTesuji.vue';
import ListeChoixMultiple from '@/views/ModeJeu/choixmultiple/ListeChoixMultiple.vue';
import LireChoixMultiple from '@/views/ModeJeu/choixmultiple/LireChoixMultiple.vue';
import SuiviEleve from '@/views/Enseignement/suivieleve/SuiviEleve.vue';
import NouveauxProblemes from '@/views/apprentissage/NouveauxProblemes.vue';
import Revision from '@/views/apprentissage/Revision.vue';
import DetailEleve from '@/views/Enseignement/suivieleve/DetailEleve.vue';
import ModificationLecon from '@/views/Enseignement/lecon/ModificationLecon.vue';
import ListeLecons from '@/views/Enseignement/lecon/ListeLecons.vue';
import VisualisationLecon from '@/views/Enseignement/lecon/VisualisationLecon.vue';
import Classe from '@/views/Enseignement/classe/Classe.vue';
import ListeClasse from '@/views/Enseignement/classe/ListeClasse.vue';
import Administration from '@/views/administration/Administration.vue';
import AffectationClub from '@/views/administration/AffectationClub.vue';
import GestionClub from '@/views/administration/GestionClub.vue';
import DemandeInscription from '@/views/administration/DemandeInscription.vue';
import Lexique from '@/views/commun/Lexique.vue';
import OuJouer from '@/views/commun/OuJouer.vue';
import KGSExplications from '@/views/commun/servers/KGS.vue';
import OGSExplications from '@/views/commun/servers/OGS.vue';
import DGSExplications from '@/views/commun/servers/DGS.vue';
import PandanetExplications from '@/views/commun/servers/Pandanet.vue';
import TygemExplications from '@/views/commun/servers/Tygem.vue';
import FoxExplications from '@/views/commun/servers/Fox.vue';
import YikesExplications from '@/views/commun/servers/Yikes.vue';
import EditionProbleme from '@/views/Enseignement/editerprobleme/EditionProbleme.vue';
import Presentation from '@/components/presentation/PresentationProbleme.vue';
import Erreur from '@/views/Erreur.vue';

import InformationsOfficielles from '@/views/commun/InformationsOfficielles.vue';
import SuivreActualites from '@/views/commun/SuivreActualites.vue';
import AuthSuccess from '@/views/auth/AuthSuccess.vue';
import SaisieNouveauMotDePasse from '@/views/auth/SaisieNouveauMotDePasse.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
    {
        path: '/',
        component: Accueil,
        name: 'Accueil'
    },
    {
        path: "/error",
        component: Erreur,
        name: 'Erreur'
    },
    {
        path: '/commentcamarche',
        component: CommentCaMarche,
        name: 'CommentCaMarche'
    },
    {
        path: '/lexique',
        component: Lexique,
        name: 'Lexique'
    },
    {
        path: '/oujouer',
        component: OuJouer,
        name: 'OuJouer'
    },
    {
        path: '/kgs',
        component: KGSExplications,
        name: 'KGS'
    },
    {
        path: '/ogs',
        component: OGSExplications,
        name: 'OGS'
    },
    {
        path: '/dgs',
        component: DGSExplications,
        name: 'DGS'
    },
    {
        path: '/pandanet',
        component: PandanetExplications,
        name: 'Pandanet'
    },
    {
        path: '/fox',
        component: FoxExplications,
        name: 'Fox'
    },
    {
        path: '/tygem',
        component: TygemExplications,
        name: 'Tygem'
    },
    {
        path: '/yikes',
        component: YikesExplications,
        name: 'Yikes'
    },
    {
        path: '/informationsofficielles',
        component: InformationsOfficielles,
        name: 'InformationsOfficielles'
    },
    {
        path: '/suivreactualites',
        component: SuivreActualites,
        name: 'SuivreActualites'
    },
    {
        path: '/profil',
        component: Profil,
        name: 'Profil'
    },
    {
        path: '/libertes/:id(\\d+)',
        component: LireProblemeLibertes,
        name: 'Liberte',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/tesuji/:id(\\d+)',
        component: LireTesuji,
        name: 'Tesuji',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/choixmultiple/:id(\\d+)',
        component: LireChoixMultiple,
        name: 'ChoixMultiple',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/suivieleve/',
        component: SuiviEleve,
        name: 'SuiviEleve',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/nouveauxproblemes/',
        component: NouveauxProblemes,
        name: 'NouveauxProblemes',
        beforeEnter: authGuard
    },
    {
        path: '/revisions/',
        component: Revision,
        name: 'Revision',
        beforeEnter: authGuard
    },
    {
        path: '/suivieleve/:id(\\d+)',
        component: DetailEleve,
        name: 'DetailEleve',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/creationLecon/',
        component: ModificationLecon,
        name: 'CreationLecon',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/modificationLecon/:id(\\d+)',
        component: ModificationLecon,
        name: 'ModificationLecon',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/lecons/',
        component: ListeLecons,
        name: 'ListeLecons',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/lecon/:id(\\d+)',
        component: VisualisationLecon,
        name: 'VisualisationLecon',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/classe/',
        component: Classe,
        name: 'Classe',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/classe/:id(\\d+)',
        component: Classe,
        name: 'VisualisationClasse',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/classes/',
        component: ListeClasse,
        name: 'ListeClasse',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/administration/',
        component: Administration,
        name: 'Administration',
        beforeEnter: authGuardAdmin
    },
    {
        path: '/affectationclub/:id(\\d+)',
        component: AffectationClub,
        name: 'AffectationClub',
        beforeEnter: authGuardAdmin
    },
    {
        path: '/gestionClub/',
        component: GestionClub,
        name: 'GestionClub',
        beforeEnter: authGuardProfesseur
    },
    {
        path: '/demandeInscription/',
        component: DemandeInscription,
        name: 'DemandeInscription',
        beforeEnter: authGuard
    },
    {
        path: '/editionprobleme/:idLecon(\\d+)',
        component: EditionProbleme,
        name: 'CreationProbleme'
    },
    {
        path: '/editionprobleme/:idProbleme(\\d+)',
        component: EditionProbleme,
        name: 'EditionProbleme'
    },
    {
        path: '/presentation/',
        component: Presentation,
        name: 'Presentation'
    },
    {
        path: '/auth-success/',
        component: AuthSuccess,
        name: 'ConnexionGoogle'
    },
    {
        path: '/nouveau-mot-de-passe/:idDemande',
        component: SaisieNouveauMotDePasse,
        name: 'SaisieNouveauMotDePasse'
    },
    {
        path: '*',
        redirect: '/'
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.onError((error) => {
    console.error("Erreur détectée :", error);
    router.push({path: '/error'});
  });

export default router;
