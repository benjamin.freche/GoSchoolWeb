import '@/class-component-hooks';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import config from './config';
import { AuthPlugin } from './auth/auth';
import GoogleAuth from 'vue-google-auth'

Vue.config.productionTip = false;
Vue.use(config);
Vue.use(AuthPlugin);

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');
