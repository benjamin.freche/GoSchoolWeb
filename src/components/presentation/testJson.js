export const TEST = {
    id: 14,
    reference: null,
    situationInitiale: [
        {
            abscisse: 4,
            ordonnee: 3,
            commentaire: null,
            couleur: 'BLANC'
        },
        {
            abscisse: 3,
            ordonnee: 2,
            commentaire: null,
            couleur: 'NOIR'
        },
        {
            abscisse: 3,
            ordonnee: 3,
            commentaire: null,
            couleur: 'BLANC'
        },
        {
            abscisse: 2,
            ordonnee: 2,
            commentaire: null,
            couleur: 'NOIR'
        },
        {
            abscisse: 2,
            ordonnee: 3,
            commentaire: null,
            couleur: 'BLANC'
        },
        {
            abscisse: 1,
            ordonnee: 2,
            commentaire: null,
            couleur: 'NOIR'
        },
        {
            abscisse: 1,
            ordonnee: 3,
            commentaire: null,
            couleur: 'BLANC'
        },
        {
            abscisse: 5,
            ordonnee: 1,
            commentaire: null,
            couleur: 'BLANC'
        },
        {
            abscisse: 5,
            ordonnee: 2,
            commentaire: null,
            couleur: 'BLANC'
        },
        {
            abscisse: 4,
            ordonnee: 1,
            commentaire: null,
            couleur: 'NOIR'
        },
        {
            abscisse: 5,
            ordonnee: 3,
            commentaire: null,
            couleur: 'BLANC'
        },
        {
            abscisse: 4,
            ordonnee: 2,
            commentaire: null,
            couleur: 'NOIR'
        }
    ],
    marqueursInitiaux: [],
    niveau: '2k',
    position: {
        espaceDepuisHaut: 0,
        espaceDepuisBas: 11,
        espaceDepuisGauche: 0,
        espaceDepuisDroite: 11
    },
    typeProbleme: 'TESUJI',
    commentaire: null,
    possibilites: [
        {
            coups: [
                {
                    abscisse: 2,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 4,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 5,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 2,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 3,
                    commentaire: null,
                    couleur: null
                }
            ],
            resultat: false,
            marqueurs: []
        },
        {
            coups: [
                {
                    abscisse: 2,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 4,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 5,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 1,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 2,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                }
            ],
            resultat: false,
            marqueurs: []
        },
        {
            coups: [
                {
                    abscisse: 2,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 4,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 1,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 2,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                }
            ],
            resultat: false,
            marqueurs: []
        },
        {
            coups: [
                {
                    abscisse: 1,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 2,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 3,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 2,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 4,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 5,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                }
            ],
            resultat: false,
            marqueurs: []
        },
        {
            coups: [
                {
                    abscisse: 1,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 2,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 4,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 5,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 1,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 3,
                    commentaire: null,
                    couleur: null
                }
            ],
            resultat: false,
            marqueurs: []
        },
        {
            coups: [
                {
                    abscisse: 1,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 2,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 4,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 5,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 2,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 3,
                    commentaire: null,
                    couleur: null
                }
            ],
            resultat: false,
            marqueurs: []
        },
        {
            coups: [
                {
                    abscisse: 1,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 2,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 2,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 1,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 4,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 2,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                }
            ],
            resultat: false,
            marqueurs: []
        },
        {
            coups: [
                {
                    abscisse: 1,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 2,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 2,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 1,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 4,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 2,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 5,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                }
            ],
            resultat: true,
            marqueurs: []
        },
        {
            coups: [
                {
                    abscisse: 0,
                    ordonnee: 2,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 4,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 5,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 2,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                }
            ],
            resultat: false,
            marqueurs: []
        },
        {
            coups: [
                {
                    abscisse: 2,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 4,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 1,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 5,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 2,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 3,
                    commentaire: null,
                    couleur: null
                }
            ],
            resultat: false,
            marqueurs: []
        },
        {
            coups: [
                {
                    abscisse: 1,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 2,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 1,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 0,
                    ordonnee: 3,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 2,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 4,
                    ordonnee: 5,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 3,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                },
                {
                    abscisse: 5,
                    ordonnee: 0,
                    commentaire: null,
                    couleur: null
                }
            ],
            resultat: false,
            marqueurs: []
        }
    ]
};
