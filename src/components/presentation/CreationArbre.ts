import Possibilite from '../../classes/Possibilite';
import Tesuji from '../../classes/Tesuji';
import Coup from '../../classes/Coup';
import Noeud, { ResultatNoeudPossible } from './Noeud';

export default class CreationArbre {
    public creerProbleme(probleme: Tesuji): Noeud[] {
        return this.creer(probleme.getPossibilites());
    }

    public creer(possibilites: Possibilite[]): Noeud[] {
        const noeuds = this.getNoeudsPourIndex(possibilites, possibilites, 0);
        return noeuds;
    }

    private getNoeudsPourIndex(
        possibilites: Possibilite[],
        possibilitesTotales: Possibilite[],
        index: number
    ): Noeud[] {
        let coups: Coup[] = possibilites
            .filter(possibilite => index <= possibilite.getCoups().length - 1)
            .map(possibilite => possibilite.getCoups()[index]);
        coups = this.retirerDoublons(coups);

        const noeuds = [];
        coups.forEach(coup => {
            const poss = possibilites.filter(possibilite =>
                coup.isEqual(possibilite.getCoups()[index])
            );
            const noeud = new Noeud();
            noeud.setIndex(index);
            if (poss.every(p => p.getResultat())) {
                noeud.setResultat(ResultatNoeudPossible.VALIDE);
            } else if (poss.every(p => !p.getResultat())) {
                noeud.setResultat(ResultatNoeudPossible.INVALIDE);
            } else {
                noeud.setResultat(ResultatNoeudPossible.NON_DEFINI);
            }
            const numeroPossibilite = poss.map(possibilite =>
                possibilitesTotales.indexOf(possibilite)
            );
            noeud.setPossibilites(numeroPossibilite);
            noeuds.push(noeud);
            this.creerDepuisNoeud(noeud, poss, possibilitesTotales);
        });
        return noeuds;
    }

    private retirerDoublons(coups: Coup[]): Coup[] {
        return coups.filter(
            (thing, index, self) =>
                index === self.findIndex(t => t.isEqual(thing))
        );
    }

    private creerDepuisNoeud(
        noeudPrecedent: Noeud,
        possibilites: Possibilite[],
        possibilitesTotales: Possibilite[]
    ) {
        const index = noeudPrecedent.getIndex() + 1;
        noeudPrecedent.setNoeudsSuivants(
            this.getNoeudsPourIndex(possibilites, possibilitesTotales, index)
        );
    }
}
