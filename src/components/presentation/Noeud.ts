export enum ResultatNoeudPossible {
    VALIDE = 0,
    INVALIDE = 1,
    NON_DEFINI = 2
}
export default class Noeud {
    private possibilites: number[];
    private index: number;
    private resultat: ResultatNoeudPossible;
    private noeudsSuivants: Noeud[];

    public setIndex(index: number) {
        this.index = index;
    }

    public getIndex(): number {
        return this.index;
    }

    public setPossibilites(possibilites: number[]) {
        this.possibilites = possibilites;
    }

    public getPossibilites(): number[] {
        return this.possibilites;
    }

    public setResultat(value: ResultatNoeudPossible) {
        this.resultat = value;
    }

    public getResultat(): ResultatNoeudPossible {
        return this.resultat;
    }

    public setNoeudsSuivants(noeudsSuivants: Noeud[]) {
        this.noeudsSuivants = noeudsSuivants;
    }

    public getNoeudsSuivants(): Noeud[] {
        return this.noeudsSuivants;
    }
}
