const coordonnees = {
    // draw on grid layer
    grid: {
        draw: function(args, board) {
            let ch, t;

            this.fillStyle = 'rgba(0,0,0,0.7)';
            this.textBaseline = 'middle';
            this.textAlign = 'center';
            this.font = board.stoneRadius + 'px ' + (board.font || '');

            const xright = board.getX(-0.75);
            const xleft = board.getX(board.size - 0.25);
            const ytop = board.getY(-0.75);
            const ybottom = board.getY(board.size - 0.25);

            for (let i = 0; i < board.size; i++) {
                ch = i + 'A'.charCodeAt(0);
                if (ch >= 'I'.charCodeAt(0)) ch++;

                t = board.getY(i);
                this.fillText(board.size - i, xright, t);
                this.fillText(board.size - i, xleft, t);

                t = board.getX(i);
                this.fillText(String.fromCharCode(ch), t, ytop);
                this.fillText(String.fromCharCode(ch), t, ybottom);
            }

            this.fillStyle = 'black';
        }
    }
};

export default coordonnees;
