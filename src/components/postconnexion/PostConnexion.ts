import Vue from 'vue';
import Axios from '@/auth/axiosAuth';

let PostConnexion;

export default PostConnexion = new Vue({
    methods: {
        effectuerPostConnexion(vue: Vue) {
            Axios.get('lexique/', response => {
                Vue.prototype.$lexique = response.data;
            });
        }
    }
});
