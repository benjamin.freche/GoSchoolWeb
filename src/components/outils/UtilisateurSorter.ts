import Vue from 'vue';
import Utilisateur from '@/classes/Utilisateur';

let UtilisateurSorter;

export default UtilisateurSorter = new Vue({
    methods: {
        comparerUtilisateurs(utilisateurs: Utilisateur[]) {
            utilisateurs.sort(this.comparerUtilisateur);
        },

        comparerListeSelonUtilisateur(
            elements: Record<string, any>[],
            getter: (element: Record<string, any>) => Record<string, any>
        ) {
            elements.sort((x, y) =>
                this.comparerUtilisateur(getter(x), getter(y))
            );
        },

        comparerUtilisateur(
            utilisateur1: Utilisateur,
            utilisateur2: Utilisateur
        ) {
            const resultatNom = this.comparerString(
                utilisateur1.getNom(),
                utilisateur2.getNom()
            );
            return resultatNom == 0
                ? this.comparerString(
                      utilisateur1.getPrenom(),
                      utilisateur2.getPrenom()
                  )
                : resultatNom;
        },

        comparerString(x: string, y: string) {
            const upperX = x.toUpperCase();
            const upperY = y.toUpperCase();
            return upperX == upperY ? 0 : upperX > upperY ? 1 : -1;
        }
    }
});
