import TraductionLexique from '../TraductionLexique';

it('si pas d élement dans le lexique, alors retourne le même texte', () => {
    TraductionLexique.$lexique = [];
    expect(TraductionLexique.expliquerLexique('test')).toBe('test');
});

it('si élement dans le lexique inégal, alors retourne le même texte', () => {
    const element = { original: 'test', romanji: 'gru' };
    TraductionLexique.$lexique = [element];
    expect(TraductionLexique.expliquerLexique('plop')).toBe('plop');
});

describe('si élement dans le lexique trouvé', () => {
    const element = { original: 'test', romanji: 'gru' };
    TraductionLexique.$lexique = [element];
    test('et suivi d un espace, alors retourne texte avec description', () => {
        expect(TraductionLexique.expliquerLexique('gru')).toBe('gru');
    });
    test('et suivi d un espace, alors retourne texte avec description', () => {
        expect(TraductionLexique.expliquerLexique('gru ')).toBe(
            '<span class="uk-inline"><a>gru</a><span uk-dropdown="mode: click"><b>Japonais - test</b><br>undefined</span></span>&nbsp;'
        );
    });
    test('et suivi d un point, alors retourne texte avec description', () => {
        expect(TraductionLexique.expliquerLexique('gru.')).toBe(
            '<span class="uk-inline"><a>gru</a><span uk-dropdown="mode: click"><b>Japonais - test</b><br>undefined</span></span>.'
        );
    });
    test('et suivi d une virgule, alors retourne texte avec description', () => {
        expect(TraductionLexique.expliquerLexique('gru,')).toBe(
            '<span class="uk-inline"><a>gru</a><span uk-dropdown="mode: click"><b>Japonais - test</b><br>undefined</span></span>,'
        );
    });
    test('et suivi d un point virgule, alors retourne texte avec description', () => {
        expect(TraductionLexique.expliquerLexique('gru;')).toBe(
            '<span class="uk-inline"><a>gru</a><span uk-dropdown="mode: click"><b>Japonais - test</b><br>undefined</span></span>;'
        );
    });

    test('et suivi d une (, alors retourne texte avec description', () => {
        expect(TraductionLexique.expliquerLexique('gru(')).toBe(
            '<span class="uk-inline"><a>gru</a><span uk-dropdown="mode: click"><b>Japonais - test</b><br>undefined</span></span>('
        );
    });
    test('et suivi d une ), alors retourne texte avec description', () => {
        expect(TraductionLexique.expliquerLexique('gru)')).toBe(
            '<span class="uk-inline"><a>gru</a><span uk-dropdown="mode: click"><b>Japonais - test</b><br>undefined</span></span>)'
        );
    });
    test('et suivi d un !, alors retourne texte avec description', () => {
        expect(TraductionLexique.expliquerLexique('gru!')).toBe(
            '<span class="uk-inline"><a>gru</a><span uk-dropdown="mode: click"><b>Japonais - test</b><br>undefined</span></span>!'
        );
    });

    test('et suivi d un ?, alors retourne texte avec description', () => {
        expect(TraductionLexique.expliquerLexique('gru?')).toBe(
            '<span class="uk-inline"><a>gru</a><span uk-dropdown="mode: click"><b>Japonais - test</b><br>undefined</span></span>?'
        );
    });
    test('et suivi d un -, alors retourne texte avec description', () => {
        expect(TraductionLexique.expliquerLexique('gru-')).toBe(
            '<span class="uk-inline"><a>gru</a><span uk-dropdown="mode: click"><b>Japonais - test</b><br>undefined</span></span>-'
        );
    });
});
