import ChoixMultiple from '@/classes/ChoixMultiple';
import ComptageLibertes from '@/classes/ComptageLibertes';
import Joseki from '@/classes/Joseki';
import Presentation from '@/classes/Presentation';
import Probleme from '@/classes/Probleme';
import Tesuji from '@/classes/Tesuji';
import Tsumego from '@/classes/Tsumego';
import * as TypeDeProbleme from '@/components/constantes/TypeDeProbleme';

export default function instancierProbleme(
    json: Record<string, any>
): Probleme {
    let probleme;
    if (json.typeProbleme === TypeDeProbleme.TESUJI) {
        probleme = new Tesuji();
    } else if (json.typeProbleme === TypeDeProbleme.CHOIXMULTIPLE) {
        probleme = new ChoixMultiple();
    } else if (json.typeProbleme === TypeDeProbleme.LIBERTES) {
        probleme = new ComptageLibertes();
    } else if (json.typeProbleme === TypeDeProbleme.TSUMEGO) {
        probleme = new Tsumego();
    } else if (json.typeProbleme === TypeDeProbleme.PRESENTATION) {
        probleme = new Presentation();
    } else if (json.typeProbleme === TypeDeProbleme.JOSEKI) {
        probleme = new Joseki();
    }
    probleme.mapperJson(json);
    return probleme;
}
