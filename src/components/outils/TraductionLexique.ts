import Vue from 'vue';
import { ElementLexique } from 'vue/types/vue';

let TraductionLexique;

export default TraductionLexique = new Vue({
    methods: {
        expliquerLexique(texte: string): string {
            const reducer = (st, element) =>
                this.remplacerUnElement(st, element);
            return this.$lexique.reduce(reducer, texte);
        },

        remplacerUnElement(texte: string, element: ElementLexique): string {
            let resultat = this.remplacerUneLangue(
                texte,
                element,
                element.hanyu,
                'Chinois'
            );
            resultat = this.remplacerUneLangue(
                resultat,
                element,
                element.pinyin,
                'Chinois'
            );
            resultat = this.remplacerUneLangue(
                resultat,
                element,
                element.kanji,
                'Japonais'
            );
            resultat = this.remplacerUneLangue(
                resultat,
                element,
                element.romanji,
                'Japonais'
            );
            resultat = this.remplacerUneLangue(
                resultat,
                element,
                element.hangeul,
                'Coréen'
            );
            return this.remplacerUneLangue(
                resultat,
                element,
                element.romanisationCoreen,
                'Coréen'
            );
        },

        remplacerUneLangue(
            texte: string,
            element: ElementLexique,
            expression: string,
            langue: string
        ): string {
            texte = this.remplacerElementPourUneLangue(
                texte,
                element,
                expression,
                langue,
                ' ',
                '&nbsp;'
            );
            texte = this.remplacerElementPourUneLangue(
                texte,
                element,
                expression,
                langue,
                '.'
            );
            texte = this.remplacerElementPourUneLangue(
                texte,
                element,
                expression,
                langue,
                '?'
            );
            texte = this.remplacerElementPourUneLangue(
                texte,
                element,
                expression,
                langue,
                '!'
            );
            texte = this.remplacerElementPourUneLangue(
                texte,
                element,
                expression,
                langue,
                ','
            );
            texte = this.remplacerElementPourUneLangue(
                texte,
                element,
                expression,
                langue,
                ';'
            );
            texte = this.remplacerElementPourUneLangue(
                texte,
                element,
                expression,
                langue,
                '('
            );
            texte = this.remplacerElementPourUneLangue(
                texte,
                element,
                expression,
                langue,
                ')'
            );
            texte = this.remplacerElementPourUneLangue(
                texte,
                element,
                expression,
                langue,
                '-'
            );
            return texte;
        },

        remplacerElementPourUneLangue(
            texte: string,
            element: ElementLexique,
            expression: string,
            langue: string,
            suffixe: string,
            caractereSpecial?: string
        ): string {
            const mot = expression + suffixe;
            const nouveauSuffixe = caractereSpecial
                ? caractereSpecial
                : suffixe;
            return texte.replaceAll(
                mot,
                this.creerDropDown(
                    mot,
                    element,
                    langue,
                    nouveauSuffixe,
                    expression
                )
            );
        },

        creerDropDown(
            expression: string,
            element: ElementLexique,
            langue: string,
            suffixe: string,
            remplacerPar: string
        ): string {
            const remplacement =
                remplacerPar != null ? remplacerPar : expression;
            return (
                '<span class="uk-inline">' +
                '<a>' +
                remplacement +
                '</a>' +
                '<span uk-dropdown="mode: click">' +
                '<b>' +
                langue +
                ' - ' +
                element.original +
                '</b><br>' +
                element.description +
                '</span></span>' +
                suffixe
            );
        }
    }
});
