export const TESUJI = 'TESUJI';
export const LIBERTES = 'LIBERTES';
export const CHOIXMULTIPLE = 'CHOIXMULTIPLE';
export const TSUMEGO = 'TSUMEGO';
export const JOSEKI = 'JOSEKI';
export const PRESENTATION = 'PRESENTATION';

export function isAvecPossibilitesStandard(type: string): boolean {
    return type === TESUJI || type === TSUMEGO || type === JOSEKI;
}

export function getNomAdresse(type: string): string {
    if (type === LIBERTES) {
        return 'libertes';
    } else if (type === CHOIXMULTIPLE) {
        return 'choixmultiple';
    } else if (type === TESUJI) {
        return 'tesuji';
    } else if (type === TSUMEGO) {
        return 'tsumego';
    } else if (type === PRESENTATION) {
        return 'presentation';
    } else if (type === JOSEKI) {
        return 'joseki';
    }
}
