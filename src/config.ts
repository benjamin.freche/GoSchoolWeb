const protoInstall = {
    install: (Vue: any, options: any) => {
        Vue.prototype.$hostname = process.env.VUE_APP_HOSTNAME;
        Vue.prototype.$websocket = process.env.VUE_APP_WEBSOCKET;
        Vue.prototype.$images = process.env.VUE_APP_IMAGES;
    },
    resolveJsonModule: true
};

export default protoInstall;
