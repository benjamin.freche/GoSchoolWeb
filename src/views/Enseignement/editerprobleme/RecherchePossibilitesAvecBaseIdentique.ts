import Possibilite from '@/classes/Possibilite';
import { getInstanceEtatEdition } from './EtatEdition';

export default function rechercherPossibilitesAvecBaseIdentique(
    possibiliteCourante?: Possibilite,
    index?: number
): Possibilite[] {
    const etatEdition = getInstanceEtatEdition();
    const courante = possibiliteCourante
        ? possibiliteCourante
        : etatEdition.possibiliteEnCours;
    const indexPierre = index ? index : etatEdition.getIndexPierre();
    const resultat = etatEdition.possibilites.filter(possibilite =>
        courante.isPossibiliteAvecDebutIdentique(possibilite, indexPierre)
    );
    return resultat;
}
