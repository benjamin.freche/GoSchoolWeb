export function dataURLtoBlob(dataURL): Blob {
    const binary = atob(dataURL.split(',')[1]);
    const array = [];
    for (let i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], { type: 'image/png' });
}

import html2canvas from 'html2canvas';

export default function creerImageGoban(resolve) {
    setTimeout(() => {
        //pour être sûr que la situation intiale soit bien sur le canvas caché
        const div = document.getElementById('goban-capture-image');
        html2canvas(div, {
            onclone: function(clonedDoc) {
                clonedDoc.getElementById('goban-capture-image').style.display =
                    'block';
            }
        }).then(canvas => {
            const data = canvas.toDataURL('image/jpg');
            resolve(dataURLtoBlob(data));
        });
    }, 100);
}
