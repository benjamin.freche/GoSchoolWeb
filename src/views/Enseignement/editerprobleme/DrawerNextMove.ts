const DrawerNextMove = {
    stone: {
        draw: function(args, board) {
            const xr = board.getX(args.x),
                yr = board.getY(args.y),
                sr = board.stoneRadius / 2;

            const radgrad = this.createRadialGradient(
                xr - (2 * sr) / 5,
                yr - (2 * sr) / 5,
                2,
                xr - sr / 5,
                yr - sr / 5,
                (4 * sr) / 5
            );

            if (args.c == true) {
                radgrad.addColorStop(0, '#69FC24');
                radgrad.addColorStop(1, '#41AD0F');
            } else if (args.c == false) {
                radgrad.addColorStop(0, '#FC2D24');
                radgrad.addColorStop(1, '#9E1A14');
            } else {
                radgrad.addColorStop(0, '#355CFC');
                radgrad.addColorStop(1, '#0E2EB4');
            }

            // paint stone
            this.beginPath();
            this.fillStyle = radgrad;
            this.arc(xr - 0.5, yr - 0.5, sr - 0.5, 0, 2 * Math.PI, true);
            this.fill();
        }
    }
};

export default DrawerNextMove;
