import Coup from '@/classes/Coup';
import Possibilite from '@/classes/Possibilite';
import DrawerNextMove from './DrawerNextMove';
import rechercherPossibiliteAvecBaseIdentique from './RecherchePossibilitesAvecBaseIdentique';
import { getInstanceEtatEdition } from './EtatEdition';

function calculerResultatAAfficher(possibilite: Possibilite): boolean {
    const etatEdition = getInstanceEtatEdition();
    if (etatEdition.isPresentation()) {
        return null;
    }
    const possibilitesIdentiques = rechercherPossibiliteAvecBaseIdentique(
        possibilite,
        etatEdition.getIndexPierre() + 1
    );
    const resultatPositif = possibilitesIdentiques.some(poss =>
        poss.getResultat()
    );
    const resultatNegatif = possibilitesIdentiques.some(
        poss => !poss.getResultat()
    );
    return resultatPositif && resultatNegatif
        ? null
        : possibilite.getResultat();
}

function afficherSurGoban(board: any, nextCoup: Coup, resultat: boolean) {
    board.addObject({
        x: nextCoup.getX(),
        y: nextCoup.getY(),
        c: resultat,
        type: DrawerNextMove
    });
}

export default function previsualiserCoupsSuivants(board: any): Coup[] {
    const etatEdition = getInstanceEtatEdition();
    const possibilitesAUtiliser = rechercherPossibiliteAvecBaseIdentique();
    const nextCoups = possibilitesAUtiliser
        .filter(
            possibilite =>
                possibilite.getCoups().length > etatEdition.getIndexPierre() + 1
        )
        .map(possibilite => {
            const nextCoup = possibilite.getCoups()[
                etatEdition.getIndexPierre() + 1
            ];
            return {
                next: nextCoup,
                resultat: calculerResultatAAfficher(possibilite)
            };
        });
    nextCoups.forEach(element =>
        afficherSurGoban(board, element.next, element.resultat)
    );
    return nextCoups.map(element => element.next);
}
