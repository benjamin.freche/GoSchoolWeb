import Possibilite from '@/classes/Possibilite';
import { getInstanceEtatEdition } from './EtatEdition';

function supprimerVariationsFuturesDevenuesDoublon(possibilitesModifiees) {
    const etatEdition = getInstanceEtatEdition();
    if (possibilitesModifiees.length > 1) {
        const doublon = possibilitesModifiees.filter(
            poss => poss !== etatEdition.possibiliteEnCours
        );
        etatEdition.possibilites = etatEdition.possibilites.filter(
            possibilite => !doublon.some(poss => poss === possibilite)
        );
    }
}

function getDoublon(): Possibilite {
    const etatEdition = getInstanceEtatEdition();
    const possibilite = etatEdition.possibiliteEnCours;
    const doublons = etatEdition.possibilites.filter(
        poss =>
            possibilite !== poss &&
            possibilite.isPossibiliteAvecDebutIdentique(
                poss,
                etatEdition.getIndexPierre() - 1
            )
    );
    return doublons.length > 0 ? doublons[0] : null;
}

function supprimerVariationCouranteSiDevenueDoublon() {
    const etatEdition = getInstanceEtatEdition();
    const doublon = getDoublon();
    if (doublon != null) {
        etatEdition.possibilites = etatEdition.possibilites.filter(
            possibilite => possibilite !== etatEdition.possibiliteEnCours
        );
        const index = etatEdition.possibilites.indexOf(doublon);
        etatEdition.changerPossibiliteEnCours(index);
    }
}

export default function supprimerVariation() {
    const etatEdition = getInstanceEtatEdition();
    const coupASupprimer = etatEdition.coupEnCours;
    const possibilitesAModifier = etatEdition.possibilites
        .filter(
            possibilite =>
                etatEdition.getIndexPierre() < possibilite.getCoups().length
        )
        .filter(possibilite =>
            possibilite
                .getCoups()
                [etatEdition.getIndexPierre()].isEqual(coupASupprimer)
        );
    possibilitesAModifier.forEach(possibilite =>
        possibilite.getCoups().splice(etatEdition.getIndexPierre())
    );
    supprimerVariationsFuturesDevenuesDoublon(possibilitesAModifier);
    supprimerVariationCouranteSiDevenueDoublon();
}
