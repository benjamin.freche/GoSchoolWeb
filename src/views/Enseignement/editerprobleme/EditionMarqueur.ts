import Marqueur from '@/classes/Marqueur';
import Possibilite from '@/classes/Possibilite';
import { getInstanceEtatEdition } from './EtatEdition';

export default class EtatEdition {
    private TypeMarqueurTexte = 'TEXTE';
    private TypeMarqueurNombre = 'NOMBRE';

    private goban: any;

    private = [];

    constructor(goban: any) {
        this.goban = goban;
    }

    public editerMarqueur(x: number, y: number) {
        const etatEdition = getInstanceEtatEdition();
        const marqueur = this.creerMarqueur(
            etatEdition.marqueurs,
            x,
            y,
            etatEdition.getTool(),
            etatEdition.getTypeMarqueur()
        );
        this.editerMarqueurGeneric(
            etatEdition.marqueurs,
            marqueur,
            x,
            y,
            etatEdition.getTypeMarqueur()
        );
    }

    public editerMarqueurPossibilite(
        x: number,
        y: number,
        possibilite: Possibilite
    ) {
        const etatEdition = getInstanceEtatEdition();
        const marqueur = this.creerMarqueur(
            possibilite.getMarqueurs(),
            x,
            y,
            etatEdition.getTool(),
            etatEdition.getTypeMarqueur()
        );
        marqueur.setPosition(etatEdition.getIndexPierre());
        this.editerMarqueurGeneric(
            possibilite.getMarqueurs(),
            marqueur,
            x,
            y,
            etatEdition.getTypeMarqueur()
        );
    }

    private editerMarqueurGeneric(
        marqueurs: Marqueur[],
        marqueur: Marqueur,
        x: number,
        y: number,
        typeMarqueur: string
    ) {
        if (
            this.isMemeMarqueurDejaPresentSurCase(marqueurs, x, y, typeMarqueur)
        ) {
            this.retirerMarqueurs(marqueurs, x, y);
        } else if (this.isMarqueurDejaPresentSurCase(marqueurs, x, y)) {
            this.retirerMarqueurs(marqueurs, x, y);
            this.traiterMarqueur(marqueurs, marqueur);
        } else {
            this.traiterMarqueur(marqueurs, marqueur);
        }
    }

    private isMemeMarqueurDejaPresentSurCase(
        marqueurs: Marqueur[],
        x: number,
        y: number,
        typeMarqueur: string
    ): boolean {
        return (
            marqueurs.some(
                marqueur =>
                    marqueur.isMemePosition(x, y) &&
                    marqueur.getType() === typeMarqueur
            ) ||
            (typeMarqueur == this.TypeMarqueurNombre &&
                this.getMarqueursNombre(marqueurs).some(marqueur =>
                    marqueur.isMemePosition(x, y)
                ))
        );
    }

    private isMarqueurDejaPresentSurCase(
        marqueurs: Marqueur[],
        x: number,
        y: number
    ): boolean {
        return marqueurs.some(marqueur => marqueur.isMemePosition(x, y));
    }

    private traiterMarqueur(marqueurs: Marqueur[], marqueur: Marqueur) {
        this.goban.afficherMarqueur(marqueur);
        marqueurs.push(marqueur);
    }

    private creerMarqueur(
        marqueurs: Marqueur[],
        x: number,
        y: number,
        tool: string,
        typeMarqueur: string
    ): Marqueur {
        if (typeMarqueur === this.TypeMarqueurTexte) {
            const marqueur = new Marqueur();
            marqueur.setX(x);
            marqueur.setY(y);
            marqueur.setType(typeMarqueur);
            marqueur.setRepresentation(this.getProchaineLettre(marqueurs));
            return marqueur;
        } else if (typeMarqueur === this.TypeMarqueurNombre) {
            const marqueur = new Marqueur();
            marqueur.setX(x);
            marqueur.setY(y);
            marqueur.setType(this.TypeMarqueurTexte);
            marqueur.setRepresentation(this.getProchainNombre(marqueurs));
            return marqueur;
        } else {
            const marqueur = new Marqueur();
            marqueur.setX(x);
            marqueur.setY(y);
            marqueur.setType(typeMarqueur);
            marqueur.setRepresentation(tool);
            return marqueur;
        }
    }

    private retirerMarqueurs(marqueurs: Marqueur[], x: number, y: number) {
        this.goban.getBoard().removeObject({ x: x, y: y, type: 'SQ' });
        this.goban.getBoard().removeObject({ x: x, y: y, type: 'CR' });
        this.goban.getBoard().removeObject({ x: x, y: y, type: 'TR' });
        this.goban.getBoard().removeObject({ x: x, y: y, type: 'LB' });
        marqueurs
            .filter(marqueur => marqueur.isMemePosition(x, y))
            .forEach(marqueur =>
                marqueurs.splice(marqueurs.indexOf(marqueur), 1)
            );
    }

    private getMarqueursNombre(marqueurs: Marqueur[]): Marqueur[] {
        return marqueurs
            .filter(
                marqueur =>
                    marqueur.getType() == this.TypeMarqueurTexte &&
                    !isNaN(+marqueur.getRepresentation())
            )
            .sort();
    }

    private getMarqueursText(marqueurs: Marqueur[]): Marqueur[] {
        return marqueurs
            .filter(
                marqueur =>
                    marqueur.getType() == this.TypeMarqueurTexte &&
                    isNaN(+marqueur.getRepresentation())
            )
            .sort();
    }

    private getProchaineLettre(marqueurs: Marqueur[]): string {
        const marqueursTexte = this.getMarqueursText(marqueurs)
            .map(marqueur => marqueur.getRepresentation().charCodeAt(0))
            .sort();
        const charCodeA = 'A'.charCodeAt(0);
        if (!marqueursTexte.includes(charCodeA)) {
            return 'A';
        }
        for (
            let i = charCodeA;
            i <= charCodeA + marqueursTexte.length - 1;
            i++
        ) {
            if (marqueursTexte.indexOf(i) == -1) {
                return String.fromCharCode(i);
            }
        }
        return String.fromCharCode(
            marqueursTexte[marqueursTexte.length - 1] + 1
        );
    }

    private getProchainNombre(marqueurs: Marqueur[]): string {
        const marqueursNombre = this.getMarqueursNombre(marqueurs)
            .map(marqueur => parseInt(marqueur.getRepresentation()))
            .sort();
        if (!marqueursNombre.includes(1)) {
            return '1';
        }
        for (let i = 1; i <= marqueursNombre.length; i++) {
            if (marqueursNombre.indexOf(i) == -1) {
                return i.toString();
            }
        }
        return (marqueursNombre.length + 1).toString();
    }
}
