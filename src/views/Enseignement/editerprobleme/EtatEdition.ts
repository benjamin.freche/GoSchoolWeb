// @ts-ignore
import WGo from 'wgo';
import Coup from '@/classes/Coup';
import '@/components/constantes/TypeOutilsEdition';
import * as outils from '@/components/constantes/TypeOutilsEdition';
import * as TypeProbleme from '@/components/constantes/TypeDeProbleme';
import Vue from 'vue';
import Possibilite from '@/classes/Possibilite';
import Configuration from '@/classes/Configuration';
import ComptageLibertes from '@/classes/ComptageLibertes';
import ChoixMultiple from '@/classes/ChoixMultiple';
import Tesuji from '@/classes/Tesuji';
import Tsumego from '@/classes/Tsumego';
import Presentation from '@/classes/Presentation';
import Probleme from '@/classes/Probleme';
import AvecPossibilites from '@/classes/AvecPossibilites';
import Joseki from '@/classes/Joseki';

let etatEdition;

export const newInstanceEtatEdition = () => {
    etatEdition = new Vue({
        data: {
            //transverse
            idExercice: null,
            typeProbleme: '',
            etapeConfiguration: 'CONFIGURATION',
            etapeDetail: 'DETAIL',
            etape: 'CONFIGURATION',
            outilEnCours: 'Noir',
            tool: WGo.B,
            typeOutil: outils.OUTIL_PIERRE,
            typeMarqueur: '',
            indexPierre: -1,
            indexPossibilite: 0,

            //configuration
            niveau: '20k',
            tailleGoban: '19',
            decoupageProbleme: false,
            espaceDecoupage: 2,
            tagsUtilise: [],
            titre: '',
            ordreAffichage: 0,

            //Goban
            coupEnCours: null,
            pierresNoires: [],
            pierresBlanches: [],
            marqueurs: [],
            possibiliteEnCours: null,
            possibilites: [],
            commentaireInitial: '',
            position: null,

            //details
            choixMultiple: [],

            //actions
            erreurs: []
        },

        created() {
            this.reinitialiser();
        },

        methods: {
            setTypeProbleme(type: string) {
                this.typeProbleme = type;
            },

            getTypeProbleme(): string {
                return this.typeProbleme;
            },

            isConfiguration(): boolean {
                return this.etape == this.etapeConfiguration;
            },

            isDetail(): boolean {
                return this.etape == this.etapeDetail;
            },

            isOutilPierre(): boolean {
                return this.typeOutil === outils.OUTIL_PIERRE;
            },

            isOutilMarqueur(): boolean {
                return this.typeOutil === outils.OUTIL_MARQUEUR;
            },

            setOutilEnCours(outil: string) {
                this.outilEnCours = outil;
            },

            getOutilEnCours(): string {
                return this.outilEnCours;
            },

            getTool(): string {
                return this.tool;
            },

            getTypeMarqueur(): string {
                return this.typeMarqueur;
            },

            changerEtape() {
                if (this.etape == this.etapeConfiguration) {
                    this.etape = this.etapeDetail;
                    this.setListenerPierres();
                } else {
                    this.etape = this.etapeConfiguration;
                    this.setListenerPierresNoires();
                }
            },

            getIndexPierre(): number {
                return this.indexPierre;
            },

            setIndexPierre(value: number) {
                this.indexPierre = value;
            },

            addIndexPierre() {
                this.indexPierre++;
            },

            removeIndexPierre() {
                this.indexPierre--;
            },

            getIndexPossibilite(): number {
                return this.indexPossibilite;
            },

            addIndexPossibilite() {
                this.indexPossibilite++;
            },

            removeIndexPossibilite() {
                this.indexPossibilite--;
            },

            setIndexPossibilite(value: number) {
                this.indexPossibilite = value;
            },

            setListenerPierresNoires() {
                this.setOutilEnCours('Noir');
                this.typeOutil = outils.OUTIL_PIERRE;
                this.tool = WGo.B;
            },

            setListenerPierresBlanches() {
                this.setOutilEnCours('Blanc');
                this.typeOutil = outils.OUTIL_PIERRE;
                this.tool = WGo.W;
            },

            setListenerPierres() {
                this.setOutilEnCours('Pierres');
                this.typeOutil = outils.OUTIL_PIERRE;
            },

            setListenerCarre() {
                this.setOutilEnCours('Carre');
                this.typeOutil = outils.OUTIL_MARQUEUR;
                this.tool = 'SQ';
                this.typeMarqueur = outils.TYPEMARQUEUR_CARRE;
            },

            setListenerCercle() {
                this.setOutilEnCours('Cercle');
                this.typeOutil = outils.OUTIL_MARQUEUR;
                this.tool = 'CR';
                this.typeMarqueur = outils.TYPEMARQUEUR_CERCLE;
            },

            setListenerTriangle() {
                this.setOutilEnCours('Triangle');
                this.typeOutil = outils.OUTIL_MARQUEUR;
                this.tool = 'TR';
                this.typeMarqueur = outils.TYPEMARQUEUR_TRIANGLE;
            },

            setListenerTexte() {
                this.setOutilEnCours('Texte');
                this.typeOutil = outils.OUTIL_MARQUEUR;
                this.tool = 'LB';
                this.typeMarqueur = outils.TYPEMARQUEUR_TEXTE;
            },

            setListenerNombre() {
                this.setOutilEnCours('Nombre');
                this.typeOutil = outils.OUTIL_MARQUEUR;
                this.tool = 'LB';
                this.typeMarqueur = outils.TYPEMARQUEUR_NOMBRE;
            },

            changerCouleur() {
                this.tool = this.tool === WGo.B ? WGo.W : WGo.B;
            },

            isChoixMultiple(): boolean {
                return this.getTypeProbleme() === TypeProbleme.CHOIXMULTIPLE;
            },

            isLibertes(): boolean {
                return this.getTypeProbleme() === TypeProbleme.LIBERTES;
            },

            isTesuji(): boolean {
                return this.getTypeProbleme() === TypeProbleme.TESUJI;
            },

            isTsumego(): boolean {
                return this.getTypeProbleme() === TypeProbleme.TSUMEGO;
            },

            isJoseki(): boolean {
                return this.getTypeProbleme() === TypeProbleme.JOSEKI;
            },

            isPresentation(): boolean {
                return this.getTypeProbleme() === TypeProbleme.PRESENTATION;
            },

            getConfiguration(): Configuration {
                return this.typeProbleme
                    ? new Configuration(
                          this.niveau,
                          this.tailleGoban,
                          this.decoupageProbleme ? +this.espaceDecoupage : NaN
                      )
                    : null;
            },

            getProblemeComplet(): Configuration {
                const configuration: Configuration = this.getConfiguration();
                if (configuration) {
                    this.creerProbleme(configuration);
                    const probleme = configuration.getProbleme();
                    probleme.setNiveau(configuration.getNiveau());
                    probleme.setCommentaire(this.commentaireInitial);
                    configuration.setValide(
                        this.controlerConfiguration(probleme)
                    );
                    return configuration;
                }
                this.erreurs.push("Merci de renseigner un type d'exercice");
                return null;
            },

            controlerConfiguration(probleme: Probleme): boolean {
                this.setSituationInitialeSurProbleme(probleme);
                if (probleme.getSituationInitiale().length === 0) {
                    this.erreurs = [];
                    this.erreurs.push(
                        'Le problème doit avoir au moins une pierre initiale.'
                    );
                    return false;
                }
                return true;
            },

            setSituationInitialeSurProbleme(probleme: Probleme) {
                this.pierresNoires.forEach(coup =>
                    probleme.getSituationInitiale().push(coup)
                );
                this.pierresBlanches.forEach(coup =>
                    probleme.getSituationInitiale().push(coup)
                );
                this.marqueurs.forEach(marqueur =>
                    probleme.getMarqueursInitiaux().push(marqueur)
                );
                probleme.setCommentaire(this.commentaireInitial);
            },

            creerProbleme(configuration: Configuration) {
                if (this.isLibertes()) {
                    const probleme = new ComptageLibertes();
                    probleme.setId(parseInt(this.idExercice));
                    configuration.setProbleme(probleme);
                } else if (this.isChoixMultiple()) {
                    const choixMultiple = new ChoixMultiple();
                    choixMultiple.setId(parseInt(this.idExercice));
                    configuration.setProbleme(choixMultiple);
                    configuration.setValide(
                        this.getChoixMultipleComplet(choixMultiple)
                    );
                } else if (
                    TypeProbleme.isAvecPossibilitesStandard(
                        this.getTypeProbleme()
                    )
                ) {
                    let probleme;
                    if (this.isTesuji()) {
                        probleme = new Tesuji();
                    } else if (this.isTsumego()) {
                        probleme = new Tsumego();
                    } else if (this.isJoseki()) {
                        probleme = new Joseki();
                    }
                    probleme.setId(parseInt(this.idExercice));
                    configuration.setProbleme(probleme);
                    configuration.setValide(
                        this.getAvecPossibiliteComplet(probleme)
                    );
                } else if (this.isPresentation()) {
                    const presentation = new Presentation();
                    presentation.setId(parseInt(this.idExercice));
                    configuration.setProbleme(presentation);
                    configuration.setValide(
                        this.getAvecPossibiliteComplet(presentation)
                    );
                }
            },

            getChoixMultipleComplet(probleme: Probleme): boolean {
                this.checkChoixMultipleValide();
                if (this.erreurs.length === 0) {
                    this.renseignerProbleme(probleme as ChoixMultiple);
                    return true;
                }
                return false;
            },

            renseignerProbleme(probleme: ChoixMultiple) {
                probleme.setChoix(this.choixMultiple);
                probleme.setPossibilites(this.possibilites);
            },

            checkChoixMultipleValide() {
                if (this.choixMultiple.length < 2) {
                    this.erreurs.push(
                        "Il faut au moins deux options possibles pour envoyer le problème. N'oubliez pas d'ajouter vos options."
                    );
                } else if (!this.isChoixPossedePositifEtNegatif()) {
                    this.erreurs.push(
                        'Il faut au moins un choix vrai et un faux.'
                    );
                }
                if (!this.isPossibilitesCorrectes()) {
                    this.erreurs.push(
                        "Il faut créer une seule variation. Celle-ci correspond à ce qui sera joué quand l'élève cliquera sur la bonne réponse."
                    );
                }
            },

            isChoixPossedePositifEtNegatif(): boolean {
                const isPossedeChoixPositif = this.choixMultiple.some(choix =>
                    choix.getResultat()
                );
                const isPossedeChoixNegatif = this.choixMultiple.some(
                    choix => !choix.getResultat()
                );
                return isPossedeChoixPositif && isPossedeChoixNegatif;
            },

            isPossibilitesCorrectes(): boolean {
                return (
                    this.possibilites.length === 1 &&
                    this.possibilites[0].getCoups().length > 0
                );
            },

            getAvecPossibiliteComplet(probleme: Probleme): boolean {
                if (
                    !this.isPresentation() &&
                    !this.possibilites.some(possibilite =>
                        possibilite.getResultat()
                    )
                ) {
                    this.erreurs.push(
                        "Au moins une variation doit être 'Réussi'."
                    );
                    return false;
                } else {
                    (probleme as AvecPossibilites).setPossibilites(
                        this.possibilites
                    );
                    return true;
                }
            },

            controlerSituationInitiale(): boolean {
                if (
                    this.pierresNoires.length === 0 &&
                    this.pierresBlanches.length === 0
                ) {
                    this.erreurs.push(
                        'Le problème doit avoir au moins une pierre initiale.'
                    );
                    return false;
                }
                return true;
            },

            modifierResultatPossibilite(resultat: boolean) {
                this.possibiliteEnCours.setResultat(resultat);
            },

            modifierCoupEnCours(
                modificateur: { (b: boolean): void },
                modif: boolean
            ) {
                modificateur(modif);
                this.changerCoupEnCours();
            },

            changerCoupEnCours() {
                this.coupEnCours = this.possibiliteEnCours.getCoups()[
                    this.getIndexPierre()
                ];
            },

            changerPossibiliteEnCours(index: number) {
                this.setIndexPossibilite(index);
                this.possibiliteEnCours = this.possibilites[
                    this.getIndexPossibilite()
                ];
            },

            modifierPosition(
                modificateur: { (b: boolean): void },
                modif: boolean,
                afficherPrevisu?: boolean
            ) {
                this.modifierCoupEnCours(modificateur, modif);
                afficherPrevisu =
                    afficherPrevisu === undefined ? true : afficherPrevisu;
            },

            enregistrerCommentaire(commentaire: string) {
                commentaire = commentaire
                    ? commentaire.replace(/\r?\n/g, '<br/>')
                    : commentaire;
                if (this.isConfiguration()) {
                    if (this.controlerSituationInitiale()) {
                        this.commentaireInitial = commentaire;
                    }
                } else {
                    if (this.coupEnCours) {
                        this.coupEnCours.setCommentaire(commentaire);
                    } else {
                        this.commentaireInitial = commentaire;
                    }
                }
            },

            goTo(x: number, y: number, afficherPrevisu?: boolean) {
                const modificateur = b => {
                    this.setIndexPierre(y);
                    this.changerCoupEnCours();
                    this.changerPossibiliteEnCours(x);
                };
                this.modifierPosition(modificateur, true, afficherPrevisu);
            },

            addCoup(coup: Coup) {
                this.possibiliteEnCours.getCoups().push(coup);
                this.coupEnCours = coup;
            },

            reinitialiser() {
                this.indexPierre = -1;
                this.indexPossibilite = 0;
                this.pierresNoires = [];
                this.pierresBlanches = [];
                this.marqueurs = [];
                this.possibilites = [];
                this.possibiliteEnCours = new Possibilite();
                this.possibilites.push(this.possibiliteEnCours);
                this.coupEnCours = null;
                this.commentaireInitial = null;
            },

            varierPossibiliteEnCours(positif: boolean) {
                if (positif) {
                    this.addIndexPossibilite();
                } else {
                    this.removeIndexPossibilite();
                }
                this.possibiliteEnCours = this.possibilites[
                    this.getIndexPossibilite()
                ];
                if (
                    this.getIndexPierre() >=
                    this.possibiliteEnCours.getCoups().length
                ) {
                    this.setIndexPierre(
                        this.possibiliteEnCours.getCoups().length - 1
                    );
                }
            },

            varierCoupEnCours(positif: boolean) {
                const modificateur = b => {
                    if (b) {
                        this.addIndexPierre();
                    } else {
                        this.removeIndexPierre();
                    }
                };
                this.modifierCoupEnCours(modificateur, positif);
            }
        }
    });
    return etatEdition;
};

export const getInstanceEtatEdition = () => etatEdition;
