export default class FichierImport {
    file: File;
    libelle: string;
    niveau: string;
    typeProbleme: string;
    titre = '';
    ordreAffichage = 0;
}
