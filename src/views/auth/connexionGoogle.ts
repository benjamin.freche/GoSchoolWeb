import { useAuth } from '@/auth/auth';
import Utilisateur from '@/classes/Utilisateur';

export function envoyerDemandeTokenGoogle() {
  const OAUTH2ENDPOINTROOT = 'https://accounts.google.com/o/oauth2/v2/auth?';
  const CLIENT_ID = '371613156129-9jgsbl97r20uekp9s8osb2b3m27ejch8.apps.googleusercontent.com';
  const REDIRECT_URI = process.env.VUE_APP_HOSTNAME + 'auth/google/callback';
  const STATE_PARAM = encodeURIComponent(JSON.stringify({ targetUrl: '/profil' }));
  const SCOPE_PARAM = 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile';
  const REPONSE_TYPE = 'code';

  const adresseGoogle = OAUTH2ENDPOINTROOT +
    'client_id=' + CLIENT_ID +
    '&redirect_uri=' + REDIRECT_URI +
    '&response_type=' + REPONSE_TYPE +
    '&scope=' + SCOPE_PARAM +
    '&state=' + STATE_PARAM;

  window.open(adresseGoogle, '_blank', 'width=500,height=600');

  return new Promise((resolve, reject) => {
    const popupInterval = setInterval(() => {
      const token = localStorage.getItem('dataTokenTemp');
      if (token != undefined) {
        clearInterval(popupInterval);
        resolve(true);
      }
    }, 500);
  });
}

export function useInformationsCallback() {
  const data = JSON.parse(localStorage.getItem('dataTokenTemp'));
  localStorage.removeItem('dataTokenTemp');
  const user = new Utilisateur();
  user.mapperJson(data.user);
  useAuth().login(user, data.accessToken, false);
} 
