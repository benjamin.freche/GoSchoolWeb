import AvecPossibilites from './AvecPossibilites';
import Choix from './Choix';

export default class ChoixMultiple extends AvecPossibilites {
    private choix: Choix[];

    public mapperJson(json: Record<string, any>) {
        super.mapperJson(json);
        this.setChoix(
            json.choix.map(choix => Object.assign(new Choix(), choix))
        );
    }

    public getChoix(): Choix[] {
        return this.choix;
    }

    public setChoix(choix: Choix[]) {
        this.choix = choix;
    }
}
