// @ts-ignore
import WGo from 'wgo';
export default class Coup {
    private abscisse: number;
    private ordonnee: number;
    private couleur: string;
    private commentaire: string;

    public getX(): number {
        return this.abscisse;
    }

    public getY(): number {
        return this.ordonnee;
    }

    public setX(x: number) {
        this.abscisse = x;
    }

    public setY(y: number) {
        this.ordonnee = y;
    }

    public setTool(tool: string) {
        this.couleur = tool == WGo.B ? 'NOIR' : 'BLANC';
    }

    public isMemePosition(x: number, y: number): boolean {
        return this.getX() == x && this.getY() == y;
    }

    public getTool(): string {
        return this.couleur == 'NOIR' ? WGo.B : WGo.W;
    }

    public getCouleur(): string {
        return this.couleur;
    }

    public setCouleur(couleur: string) {
        this.couleur = couleur;
    }

    public setCommentaire(value: string) {
        this.commentaire = value;
    }

    public getCommentaire(): string {
        return this.commentaire;
    }

    public isEqual(autre: Coup): boolean {
        return (
            this.getX() == autre.getX() &&
            this.getY() == autre.getY() &&
            this.getCouleur() == autre.getCouleur()
        );
    }
}
