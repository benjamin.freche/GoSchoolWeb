import Coup from './Coup';
import Probleme from './Probleme';

export default class ComptageLibertes extends Probleme {
    private solutions: Coup[];

    public mapperJson(json: Record<string, any>) {
        super.mapperJson(json);
        this.setSolutions(
            json.solutions.map(coup => Object.assign(new Coup(), coup))
        );
    }

    public setSolutions(value: Coup[]) {
        this.solutions = value;
    }

    public getSolutions(): Coup[] {
        return this.solutions;
    }
}
