export default class Choix {
    private libelle: string;
    private resultat: boolean;

    public getLibelle(): string {
        return this.libelle;
    }

    public getResultat(): boolean {
        return this.resultat;
    }

    public setLibelle(libelle: string) {
        this.libelle = libelle;
    }

    public setResultat(resultat: boolean) {
        this.resultat = resultat;
    }
}
