export default class TagProbleme {
    private libelle: string;

    public setLibelle(libelle: string) {
        this.libelle = libelle;
    }

    public getLibelle(): string {
        return this.libelle;
    }

    public mapperJson(json: Record<string, any>) {
        this.setLibelle(json.libelle);
    }
}
