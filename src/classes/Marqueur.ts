export default class Marqueur {
    abscisse: number;
    ordonnee: number;
    type: string;
    representation: string;
    position: number;

    public getX(): number {
        return this.abscisse;
    }

    public getY(): number {
        return this.ordonnee;
    }

    public setX(x: number) {
        this.abscisse = x;
    }

    public setY(y: number) {
        this.ordonnee = y;
    }

    public setType(type: string) {
        this.type = type;
    }

    public getType(): string {
        return this.type;
    }

    public setRepresentation(representation: string) {
        this.representation = representation;
    }

    public getRepresentation(): string {
        return this.representation;
    }

    public isMemePosition(x: number, y: number): boolean {
        return this.getX() == x && this.getY() == y;
    }

    public setPosition(value: number) {
        this.position = value;
    }

    public getPosition(): number {
        return this.position;
    }
}
