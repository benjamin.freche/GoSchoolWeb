import Possibilite from './Possibilite';
import Probleme from './Probleme';

export default abstract class AvecPossibilites extends Probleme {
    private possibilites: Possibilite[];

    public getPossibilites(): Possibilite[] {
        return this.possibilites;
    }

    public setPossibilites(possibilites: Possibilite[]) {
        this.possibilites = possibilites;
    }

    public mapperJson(json: Record<string, any>) {
        super.mapperJson(json);
        this.setPossibilites(
            json.possibilites.map(possibiliteJson => {
                const possibilite = new Possibilite();
                possibilite.mapperJson(possibiliteJson);
                return possibilite;
            })
        );
    }
}
