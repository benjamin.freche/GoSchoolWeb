import Coup from './Coup';
import Marqueur from './Marqueur';

export default class Possibilite {
    private coups: Coup[] = [];
    private resultat: boolean;
    private marqueurs: Marqueur[] = [];

    public mapperJson(json: Record<string, any>) {
        json.coups.forEach((coupJson, index) => {
            const coup = Object.assign(new Coup(), coupJson);
            if (!coup.getCouleur()) {
                // fait pour les exercices v1 qui n'ont pas été migrés
                if (index === 0) {
                    coup.setCouleur('NOIR');
                } else {
                    const couleur =
                        this.getCoups()[index - 1].getCouleur() === 'NOIR'
                            ? 'BLANC'
                            : 'NOIR';
                    coup.setCouleur(couleur);
                }
            }
            this.getCoups().push(coup);
        });
        this.setMarqueurs(
            json.marqueurs.map(marqueur =>
                Object.assign(new Marqueur(), marqueur)
            )
        );
        this.setResultat(json.resultat);
    }

    public setCoups(value: Coup[]) {
        this.coups = value;
    }

    public getCoups(): Coup[] {
        return this.coups;
    }

    public setResultat(value: boolean) {
        this.resultat = value;
    }

    public getResultat(): boolean {
        return this.resultat;
    }

    public setMarqueurs(value: Marqueur[]) {
        this.marqueurs = value;
    }

    public getMarqueurs(): Marqueur[] {
        return this.marqueurs;
    }

    public isPossibiliteAvecDebutIdentique(
        autre: Possibilite,
        index: number
    ): boolean {
        for (let i = 0; i <= index; i++) {
            const coup1 = this.getCoups()[i];
            const coup2 = autre.getCoups()[i];
            if (coup1 == null || coup2 == null || !coup1.isEqual(coup2)) {
                return false;
            }
        }
        return true;
    }

    public isIndexADernierePierre(index: number): boolean {
        return this.getCoups().length - 1 === index;
    }

    public clonerAIndex(index: number): Possibilite {
        const newPossibilite = new Possibilite();
        newPossibilite.setCoups(this.getCoups().slice(null, index + 1));
        const marqueurs = this.marqueurs.filter(
            marqueur => marqueur.getPosition() <= index
        );
        newPossibilite.setMarqueurs(marqueurs);
        return newPossibilite;
    }
}
