import Probleme from './Probleme';

export default class Configuration {
    private probleme: Probleme;
    private tailleGoban: string;
    private espaceDecoupage: number;
    private niveau: string;
    private valide: boolean;

    constructor(niveau: string, tailleGoban: string, espaceDecoupage: number) {
        this.niveau = niveau;
        this.tailleGoban = tailleGoban;
        this.espaceDecoupage = espaceDecoupage;
    }

    public getProbleme(): Probleme {
        return this.probleme;
    }

    public setProbleme(probleme: Probleme) {
        this.probleme = probleme;
    }

    public getTailleGoban(): string {
        return this.tailleGoban;
    }

    public getEspaceDecoupage(): number {
        return this.espaceDecoupage;
    }

    public getNiveau(): string {
        return this.niveau;
    }

    public setValide(valide: boolean) {
        if (this.valide !== undefined) {
            this.valide = this.valide && valide;
        } else {
            this.valide = valide;
        }
    }

    public isValide(): boolean {
        return this.valide;
    }
}
