import ExperienceUtilisateur from './ExperienceUtilisateur';

export default class Utilisateur {
    private id: string;
    private nom: string;
    private prenom: string;
    private nomComplet: string;
    private idClub: string;
    private experience: ExperienceUtilisateur;

    public getId(): string {
        return this.id;
    }

    public setId(value: string) {
        this.id = value;
    }

    public getNom(): string {
        return this.nom;
    }

    public setNom(value: string) {
        this.nom = value;
    }

    public getPrenom(): string {
        return this.prenom;
    }

    public setPrenom(value: string) {
        this.prenom = value;
    }

    public getNomComplet(): string {
        return this.nomComplet;
    }

    public setNomComplet(value: string) {
        this.nomComplet = value;
    }

    public setIdClub(value: string) {
        this.idClub = value;
    }

    public getIdClub() {
        return this.idClub;
    }

    public getExperience() {
        return this.experience;
    }

    public mapperJson(json: Record<string, any>) {
        this.id = json.id;
        this.nom = json.nom;
        this.prenom = json.prenom;
        this.idClub = json.idClub;
        this.nomComplet = this.nom + ' ' + this.prenom;
        this.experience = Object.assign(new ExperienceUtilisateur(), json.experience);
    }

}
