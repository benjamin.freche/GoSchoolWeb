export default class ExperienceUtilisateur {
    private tesuji: boolean;
    private libertes: boolean;
    private choixMultiple: boolean;
    private lecon: boolean;

    public getTesuji(): boolean {
        return this.tesuji;
    }

    public setTesuji(valeur: boolean): void {
        this.tesuji = valeur;
    }

    public getLibertes(): boolean {
        return this.libertes;
    }

    public setLibertes(valeur: boolean): void {
        this.libertes = valeur;
    }

    public getChoixMultiple(): boolean {
        return this.choixMultiple;
    }

    public setChoixMultiple(valeur: boolean): void {
        this.choixMultiple = valeur;
    }

    public getLecon(): boolean {
        return this.lecon;
    }

    public setLecon(valeur: boolean): void {
        this.lecon = valeur;
    }
}