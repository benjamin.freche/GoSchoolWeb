import Coup from './Coup';
import Marqueur from './Marqueur';
import TagProbleme from './TagProbleme';
export default abstract class Probleme {
    private id: number;
    public situationInitiale: Coup[] = [];
    private marqueursInitiaux: Marqueur[] = [];
    private typeProbleme: string;
    private commentaire: string;
    private niveau: string;
    private position: Record<string, any>;
    private tagsProbleme: TagProbleme[] = [];
    private titre: string;
    private ordreAffichage: number;

    public mapperJson(json: Record<string, any>) {
        this.setId(json.id);
        this.setSituationInitiale(
            json.situationInitiale.map(coup => Object.assign(new Coup(), coup))
        );
        this.setMarqueursInitiaux(
            json.marqueursInitiaux.map(marqueur =>
                Object.assign(new Marqueur(), marqueur)
            )
        );
        this.setCommentaire(json.commentaire);
        this.setNiveau(json.niveau);
        this.setTypeProbleme(json.typeProbleme);
        this.position = json.position;
        this.setTitre(json.titre);
        this.setOrdreAffichage(json.ordre);
        this.setTagsProbleme(
            json.tagsProbleme.map(tag => Object.assign(new TagProbleme(), tag))
        );
    }

    public getId(): number {
        return this.id;
    }

    public setId(value: number) {
        this.id = value;
    }

    public getSituationInitiale(): Coup[] {
        return this.situationInitiale;
    }

    public setSituationInitiale(value: Coup[]) {
        this.situationInitiale = value;
    }

    public getMarqueursInitiaux(): Marqueur[] {
        return this.marqueursInitiaux;
    }

    public setMarqueursInitiaux(value: Marqueur[]) {
        this.marqueursInitiaux = value;
    }

    public getCommentaire(): string {
        return this.commentaire;
    }
    public setCommentaire(value: string) {
        this.commentaire = value;
    }

    public getTypeProbleme(): string {
        return this.typeProbleme;
    }
    public setTypeProbleme(value: string) {
        this.typeProbleme = value;
    }

    public getNiveau(): string {
        return this.niveau;
    }

    public setNiveau(value: string) {
        this.niveau = value;
    }

    public getPosition(): Record<string, any> {
        return this.position;
    }

    public setTagsProbleme(tagsProblemes: TagProbleme[]) {
        return (this.tagsProbleme = tagsProblemes);
    }

    public getTagsProbleme(): TagProbleme[] {
        return this.tagsProbleme;
    }

    public setTitre(titre: string) {
        this.titre = titre ? titre : '';
    }

    public getTitre(): string {
        return this.titre;
    }

    public setOrdreAffichage(ordreAffichage: number) {
        this.ordreAffichage = ordreAffichage;
    }

    public getOrdreAffichage(): number {
        return this.ordreAffichage;
    }
}
