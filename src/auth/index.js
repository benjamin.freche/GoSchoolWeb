import Vue from 'vue';
import Auth0Lock from 'auth0-lock';
import Axios from './axiosAuth';
import PostConnexion from '@/components/postconnexion/PostConnexion';

let instance;

/** Returns the current instance of the SDK */
export const getInstance = () => instance;

/** Creates an instance of the Auth0 SDK. If one has already been created, it returns that instance */
export const useAuth0 = ({ ...options }) => {
    if (instance) return instance;
    instance = new Vue({
        data() {
            return {
                loading: true,
                lock: Auth0Lock,
                isAuthenticated: false,
                isCreation: false,
                roleUtilisateur: [],
                userAuth0: [],
                user: []
            };
        },
        methods: {
            isConnecte() {
                return !this.loading && this.isAuthenticated;
            },
            isRoleAutorise(roleDemande) {
                return this.roleUtilisateur.includes(roleDemande);
            },
            isRoleAdmin() {
                return (
                    this.isConnecte() && this.roleUtilisateur.includes('Admin')
                );
            },
            isRoleProfesseur() {
                return (
                    this.isConnecte() &&
                    this.roleUtilisateur.includes('Professeur')
                );
            },
            async login() {
                this.lock.show();
            },
            async logout() {
                this.lock.logout();
                this.deconnecter();
            },
            deconnecter() {
                this.isAuthenticated = false;
                this.userAuth0 = null;
                this.user = null;
                localStorage.removeItem('user');
            },
            async setActionAuthentification(vue) {
                this.lock.on('authenticated', function(authResult) {
                    localStorage.setItem('user', JSON.stringify(authResult));
                    vue.authentifier(vue, this, authResult.accessToken);
                });
            },

            async authentifier(vue, lock, accessToken) {
                lock.getUserInfo(accessToken, function(error, profile) {
                    if (error) {
                        // Handle error
                        return;
                    }
                    vue.setUser(profile);
                    const connexion = new Promise((resolve, reject) => {
                        vue.getUtilisateur(profile.sub, resolve);
                    });
                    connexion.then(value => {
                        vue.setAuthentificated(true);
                        PostConnexion.effectuerPostConnexion();
                    });
                });
            },

            async reauthentifier() {
                const user = localStorage.getItem('user');
                if (user) {
                    const token = JSON.parse(user);
                    this.authentifier(this, this.lock, token.accessToken);
                }
            },

            async setUser(infos) {
                this.userAuth0 = infos;
            },

            async setAuthentificated(authentifie) {
                this.isAuthenticated = authentifie;
            },

            getUtilisateur(authId, resolve) {
                Axios.get('utilisateur/' + authId, response => {
                    this.user = response.data;
                    let baseAdresse = 'https://' + options.domain;
                    baseAdresse = baseAdresse.replaceAll('.', ':');
                    if (!this.user) {
                        const adresseMeta = baseAdresse + '/user_metadata';

                        let nom = this.userAuth0['family_name'];
                        let prenom = this.userAuth0['given_name'];

                        if (nom == null || prenom == null) {
                            const userMetadata = this.userAuth0[adresseMeta];
                            nom = userMetadata.nom;
                            prenom = userMetadata.prenom;
                        }

                        const formData = new FormData();
                        formData.append('authId', authId);
                        formData.append('nom', nom);
                        formData.append('prenom', prenom);
                        Axios.post(
                            'utilisateur/',
                            response => (this.user = response.data),
                            formData
                        );
                    } else {
                        const adresseRole = baseAdresse + '/roles';
                        this.roleUtilisateur = this.userAuth0[adresseRole];
                    }
                    resolve(true);
                });
            }
        },

        async created() {
            const optionsLock = {
                language: 'fr',
                autoclose: 'true',
                auth: {
                    audience: options.audience
                },
                theme: {
                    //logo: 'https://example.com/logo.png',
                    primaryColor: '#31324F'
                },
                languageDictionary: {
                    title: 'Gollege'
                },
                additionalSignUpFields: [
                    {
                        name: 'nom',
                        placeholder: 'Votre nom'
                    },
                    {
                        name: 'prenom',
                        placeholder: 'Votre prénom'
                    }
                ]
            };
            this.lock = await new Auth0Lock(
                options.clientId,
                options.domain,
                optionsLock
            );
            this.setActionAuthentification(this);
            this.reauthentifier();
            this.loading = false;
        }
    });
    return instance;
};

export const Auth0Plugin = {
    install(Vue, options) {
        Vue.prototype.$auth0 = useAuth0(options);
    }
};
