import Vue from 'vue';
import Axios from 'axios';
import router from '@/router';

let AxiosAuth;

export default AxiosAuth = new Vue({
    data: {
        router: router
    },
    methods: {
        createHeader() {
            const user = localStorage.getItem('user');
            const token = JSON.parse(user);
            return {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            };
        },

        createHeaderWithFile() {
            const user = localStorage.getItem('user');
            const token = JSON.parse(user);
            return {
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'multipart/form-data'
                }
            };
        },

        get(url: string, thenFunction: any, errorFunction?: any) {
            Axios.get(this.$hostname + url, this.createHeader())
                .then(response => {
                    thenFunction(response);
                })
                .catch(error => {
                    this.gestionErrorUnauthorized(error);
                    if (errorFunction != undefined) {
                        errorFunction(error);
                    }
                });
        },

        getUnLogged(url: string, thenFunction: any, errorFunction?: any) {
            Axios.get(this.$hostname + url)
                .then(response => {
                    thenFunction(response);
                })
                .catch(error => {
                    this.gestionErrorUnauthorized(error);
                    if (errorFunction != undefined) {
                        errorFunction(error);
                    }
                });
        },

        post(url: string, thenFunction: any, data?: any, errorFunction?: any) {
            Axios.post(this.$hostname + url, data, this.createHeader())
                .then(response => {
                    if (thenFunction != null) {
                        thenFunction(response);
                    }
                })
                .catch(error => {
                    this.gestionErrorUnauthorized(error);
                    if (errorFunction != undefined) {
                        errorFunction(error);
                    }
                });
        },

        postUnLogged(url: string, thenFunction: any, data?: any, errorFunction?: any) {
            Axios.post(this.$hostname + url, data)
                .then(response => {
                    if (thenFunction != null) {
                        thenFunction(response);
                    }
                })
                .catch(error => {
                    this.gestionErrorUnauthorized(error);
                    if (errorFunction != undefined) {
                        errorFunction(error);
                    }
                });
        },

        postConnexion(url: string, thenFunction: any, data?: any, errorFunction?: any) {
            Axios.post(this.$hostname + url, data)
                .then(response => {
                    if (thenFunction != null) {
                        thenFunction(response);
                    }
                })
                .catch(error => {
                    if (errorFunction != undefined) {
                        errorFunction(error);
                    }
                });
        },

        postFile(
            url: string,
            thenFunction: any,
            data?: any,
            errorFunction?: any
        ) {
            Axios.post(this.$hostname + url, data, this.createHeaderWithFile())
                .then(response => {
                    if (thenFunction != null) {
                        thenFunction(response);
                    }
                })
                .catch(error => {
                    this.gestionErrorUnauthorized(error);
                    if (errorFunction != undefined) {
                        errorFunction(error);
                    }
                });
        },

        put(url: string, thenFunction: any, data?: any, errorFunction?: any) {
            Axios.put(this.$hostname + url, data, this.createHeader())
                .then(response => {
                    if (thenFunction != null) {
                        thenFunction(response);
                    }
                })
                .catch(error => {
                    this.gestionErrorUnauthorized(error);
                    if (errorFunction != undefined) {
                        errorFunction(error);
                    }
                });
        },

        putFile(
            url: string,
            thenFunction: any,
            data?: any,
            errorFunction?: any
        ) {
            Axios.put(this.$hostname + url, data, this.createHeaderWithFile())
                .then(response => {
                    if (thenFunction != null) {
                        thenFunction(response);
                    }
                })
                .catch(error => {
                    this.gestionErrorUnauthorized(error);
                    if (errorFunction != undefined) {
                        errorFunction(error);
                    }
                });
        },

        delete(url: string, thenFunction: any, errorFunction?: any) {
            Axios.delete(this.$hostname + url, this.createHeaderWithFile())
                .then(response => {
                    if (thenFunction != null) {
                        thenFunction(response);
                    }
                })
                .catch(error => {
                    this.gestionErrorUnauthorized(error);
                    if (errorFunction != undefined) {
                        errorFunction(error);
                    }
                });
        },

        gestionErrorUnauthorized(error) {
            if (
                error.response != null &&
                (error.response.status == 403 || error.response.status == 401)
            ) {
                this.$auth.logout();
            }
        }
    }
});
