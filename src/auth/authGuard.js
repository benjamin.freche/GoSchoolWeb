import { getInstance } from './auth.ts';

const isHabilite = (authService, condition) => {
    return condition == null || authService.isRoleAutorise(condition);
};

const fn = (authService, next, condition) => {
    const user = localStorage.getItem('user');
    if (authService.isConnecte()) {
        if (isHabilite(authService, condition)) {
            return next();
        }
    } else if (user) {
        setTimeout(() => {
            return fn(authService, next, condition);
        }, 100);
    } else {
        return next('Accueil');
    }
};

const controlerAutorisation = (to, from, next, condition) => {
    const authService = getInstance();
        return fn(authService, next, condition);
};

export const authGuard = (to, from, next) => {
    return controlerAutorisation(to, from, next, null);
};

export const authGuardAdmin = (to, from, next) => {
    return controlerAutorisation(to, from, next, 'ADMIN');
};

export const authGuardProfesseur = (to, from, next) => {
    return controlerAutorisation(to, from, next, 'PROFESSEUR');
};
