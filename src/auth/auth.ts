import Vue from 'vue';
import PostConnexion from '@/components/postconnexion/PostConnexion';
import Utilisateur from '@/classes/Utilisateur';
import router from '../router';
import UIkit from 'uikit';
import axiosAuth from './axiosAuth';

let instance;

export const getInstance = () => instance;

export const useAuth = () => {
    if (instance) return instance;
    instance = new Vue({
        data() {
            return {
                isAuthenticated: false,
                roleUtilisateur: [],
                user: Utilisateur
            };
        },
        methods: {
            login(user: Utilisateur, token: string, redirect: boolean): void {
                this.user = user;
                if(token != undefined) {
                    this.isAuthenticated = true;
                    const infoToken = this.parseJwt(token);
                    this.roleUtilisateur = infoToken.roleUtilisateur ? infoToken.roleUtilisateur : [];
                    localStorage.setItem('user', JSON.stringify(token));
                    PostConnexion.effectuerPostConnexion();
                }
                if(redirect) {
                    router.push({
                        name: 'Profil'
                    })
                }
            },
            logout(): void {
                this.user = null;
                this.isAuthenticated = false;
                this.roleUtilisateur = [];
                localStorage.removeItem('user');
                if(router.currentRoute.path !== '/') {
                    router.push('Accueil');
                }
                UIkit.notification('Vous avez été déconnecté');
            },
            reconnect() {
               const token = localStorage.getItem('user');
               if(token != undefined) {
                   const infoToken = this.parseJwt(token);
                   this.roleUtilisateur = infoToken.roleUtilisateur ? infoToken.roleUtilisateur : [];
                   const identifiant = infoToken.sub.replace('@', '%40');
                   axiosAuth.get('utilisateur/' + identifiant, next => {
                    const retour = new Utilisateur();
                    retour.mapperJson(next.data);
                    this.user = retour;
                    this.isAuthenticated = true;
                    PostConnexion.effectuerPostConnexion();
                   });
            }
            },

            parseJwt(token):string  {
                var base64Url = token.split('.')[1];
                var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
                var jsonPayload = decodeURIComponent(
                    window
                        .atob(base64)
                        .split('')
                        .map(function (c) {
                            return (
                                '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
                            );
                        })
                        .join(''),
                );
        
                return JSON.parse(jsonPayload);
            },

            isConnecte():boolean {
                return this.isAuthenticated;
            },

            isRoleAutorise(roleDemande) {
                return this.roleUtilisateur.includes(roleDemande);
            },
            isRoleAdmin() {
                return (
                    this.isConnecte() && this.roleUtilisateur.includes('Admin')
                );
            },
            isRoleProfesseur() {
                return (
                    this.isConnecte() &&
                    this.roleUtilisateur.includes('Professeur')
                );
            },
        }

    });
    return instance;
};
export const AuthPlugin = {
    install(Vue) {
        Vue.prototype.$auth = useAuth();
    }
};
