const NodePolyfillPlugin = require('node-polyfill-webpack-plugin');

module.exports = {
    lintOnSave: false,
    publicPath: process.env.VUE_APP_ROOT,
    configureWebpack: {
        externals: {
            wgo: 'WGo'
        },
        plugins: [
            new NodePolyfillPlugin(),
        ],
        resolve: {
            fallback: {
                "child_process": false,
                "tls": false,
                "net": false,
                "nock": false,
                "aws-sdk": false,
                "mock-aws-s3": false,
                // and also other packages that are not found
            }
        }
    }
};
