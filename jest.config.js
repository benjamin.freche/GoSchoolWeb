module.exports = {
    preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
    collectCoverageFrom: ['src/**/*.js', '!**/node_modules/**'],
    coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
    testEnvironment: 'node'
};
